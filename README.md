# FilmLED

Open Hardware and Software LED floodlight for filming
--> High CRI

#License

## Software

GPL3.0

[Link to computer software to simulate and control the led panel](https://git.mosad.xyz/localhorst/OpenFilmLightController)

##Hardware

CC BY-NC 4.0 

