EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ESP32-footprints-Shem-Lib:ESP32-WROOM U1
U 1 1 5C6B1427
P 3250 2350
F 0 "U1" H 3225 3737 60  0000 C CNN
F 1 "ESP32-WROOM" H 3225 3631 60  0000 C CNN
F 2 "ESP32-footprints-Lib:ESP32-WROOM" H 3600 3700 60  0001 C CNN
F 3 "" H 2800 2800 60  0001 C CNN
	1    3250 2350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
